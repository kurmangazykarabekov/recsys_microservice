FROM jupyter/all-spark-notebook:aarch64-python-3.9

COPY requirements.txt /tmp/

RUN pip install --upgrade pip \
    && pip install -r /tmp/requirements.txt
